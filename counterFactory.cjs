const counterFactory = () => {
  let counter = 0;
  const object = {
    increment: () => {
      return ++counter;
    },
    decrement: () => {
      return --counter;
    }
  };
  return object;
};

module.exports = counterFactory;
