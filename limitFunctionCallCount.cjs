const limitFunctionCallCount = (cbFunction, n) => {

if( typeof n !== 'number' || typeof cbFunction !== 'function'){
  throw ({'error':'given arguments are not right'});
}

  const cbInvokingFunction = (...arg) => {
   if (n > 0) {
     let res = cbFunction(...arg);
      n--;
      return res;
    }else{
        return null;
    }  
  };

  return cbInvokingFunction;
};

module.exports = limitFunctionCallCount;
