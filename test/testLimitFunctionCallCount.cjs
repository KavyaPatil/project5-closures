const testLimitFunctioncall = require('../limitFunctionCallCount.cjs');


try{
    const cb = (para1) => {    
       // console.log(para1,para2)
        return para1+para1;
      }

    const result = testLimitFunctioncall(cb,2);
    console.log(result(2));
    console.log(result(3));
    console.log(result(1));
    console.log(result());
}catch(err){
    console.log(err);
}
