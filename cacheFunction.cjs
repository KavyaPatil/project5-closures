const cacheFunction = (cbFunction) => {

    if( typeof cbFunction !== 'function'){
        throw ({'error':'given arguments are not right'});
     }
    
  const cacheObject = {};

  const cbInvokingFunction = (...arg) => {
    let value = JSON.stringify(arg);
    // console.log(value)
    if (value in cacheObject) {
      
      return cacheObject[value];
    } else {
      let returnedVal = cbFunction(...arg);
      cacheObject[value] = returnedVal;
      console.log(cacheObject);
     
      return returnedVal;
    }
  };

  return cbInvokingFunction;
};

module.exports = cacheFunction;
